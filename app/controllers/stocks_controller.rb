class StocksController < ApplicationController
  STOCK_PARAMS = %i(name price quantity percentage years)

  def new
    @stock = Stock.new
  end

  def create
    @stock = Stock.new(stock_params)

    if @stock.save
      redirect_to stock_path(@stock)
    else
      render 'new'
    end
  end

  def show
    @stock = Stock.find(params[:id])
  end

  def index
    @stocks = Stock.all
  end

  private

  def stock_params
    params.require(:stock).permit(*STOCK_PARAMS)
  end
end
