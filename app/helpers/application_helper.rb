module ApplicationHelper
  def number_with_two_decimals(number)
    format('%.2f', number)
  end
end
