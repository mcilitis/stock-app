module StocksHelper
  def values_array_for_chart_to_json(stock)
    stock.stock_values_each_year.map(&:last)
      .unshift(stock.name).to_json
  end
end
