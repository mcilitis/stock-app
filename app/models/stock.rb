class Stock < ActiveRecord::Base
  validates(* %i(
    name
    price
    quantity
    percentage
    years
  ), presence: true
           )
  validates(* %i(
    price
    quantity
    percentage
    years
  ), numericality: { greater_than_or_equal_to: 0 }
           )

  def total_value
    @total_value ||= (price * quantity).round(2)
  end

  def stock_values_each_year
    @stock_values_each_year ||= StockValuesCalculator.new(self).yearly_values
  end
end
