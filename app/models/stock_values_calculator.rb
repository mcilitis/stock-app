class StockValuesCalculator
  def initialize(stock_data)
    @stock_data = stock_data
  end

  def yearly_values
    values = []
    values << [0, @stock_data.total_value]

    (1..@stock_data.years).inject(@stock_data.total_value) do |total, year|
      yearly_value = total_stock_value_with_yield(total)
      values << [year, yearly_value]
      yearly_value
    end
    values
  end

  private

  def total_stock_value_with_yield(total_value)
    (total_value + ((total_value * @stock_data.percentage) / 100)).round(2)
  end
end
