require 'rails_helper.rb'

describe StockValuesCalculator do
  describe '#yearly_values' do
    subject do
      described_class.new(
        double('stock_data',
               total_value: 400,
               percentage:  3.00,
               years:       5
              )
      )
    end

    it 'returns stock values for each year' do
      expect(subject.yearly_values).to eq(
        [
          [0, 400.00],
          [1, 412.00],
          [2, 424.36],
          [3, 437.09],
          [4, 450.20],
          [5, 463.71]
        ]
      )
    end
  end
end
