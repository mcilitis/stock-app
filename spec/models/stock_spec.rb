require 'rails_helper.rb'

describe Stock do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:price) }
    it { is_expected.to validate_presence_of(:quantity) }
    it { is_expected.to validate_presence_of(:percentage) }
    it { is_expected.to validate_presence_of(:years) }
    it do
      is_expected.to validate_numericality_of(:price)
        .is_greater_than_or_equal_to(0)
    end
    it do
      is_expected.to validate_numericality_of(:quantity)
        .is_greater_than_or_equal_to(0)
    end
    it do
      is_expected.to validate_numericality_of(:percentage)
        .is_greater_than_or_equal_to(0)
    end
    it do
      is_expected.to validate_numericality_of(:years)
        .is_greater_than_or_equal_to(0)
    end
  end

  describe '#total_value' do
    subject { described_class.new(price: 2.33, quantity: 200) }

    it 'returns price multiplied by quantity' do
      expect(subject.total_value).to eq(466)
    end
  end
end
