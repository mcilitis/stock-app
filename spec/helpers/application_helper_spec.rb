require 'rails_helper.rb'

describe ApplicationHelper do
  describe '#number_with_two_decimals' do
    it 'returns number formatted with two decimals' do
      expect(number_with_two_decimals(3.1)).to eq('3.10')
      expect(number_with_two_decimals(3)).to eq('3.00')
    end
  end
end
