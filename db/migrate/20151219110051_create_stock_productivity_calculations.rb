class CreateStockProductivityCalculations < ActiveRecord::Migration
  def change
    create_table :stock_productivity_calculations do |t|
      t.string :stock_name, null: false
      t.decimal :price, null: false
      t.integer :quantity, null: false
      t.decimal :percentage, null: false
      t.integer :years, null: false

      t.timestamps null: false
    end
  end
end
