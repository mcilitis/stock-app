class RenameStockProductivityCalculationToStock < ActiveRecord::Migration
  def change
    rename_table :stock_productivity_calculations, :stocks
  end
end
