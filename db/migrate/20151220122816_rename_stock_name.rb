class RenameStockName < ActiveRecord::Migration
  def change
    rename_column :stocks, :stock_name, :name
  end
end
