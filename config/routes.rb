Rails.application.routes.draw do
  root 'stocks#index'

  resources :stocks, only: %i(new create show index)
end
