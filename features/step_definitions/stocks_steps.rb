Given(/^I open the application url$/) do
  visit root_path
end

Given(/^I must see the front page with application title "(.*?)"$/) do |title|
  expect(page).to have_title(title)
end

Given(/^I press button "(.*?)"$/) do |content|
  click_button(content)
end

Given(/^I must see the page with title "(.*?)"$/) do |title|
  expect(page).to have_title(title)
end

Given(/^I must be able to enter the following values:$/) do |table|
  table.raw.each do |field, value, _|
    fill_in field, with: value
  end
end

Given(/^I must see the original input data:$/) do |table|
  table.raw.each do |name, value|
    expect(page).to have_content("#{name}: #{value}")
  end
end

Given(/^list of stock values for each year:$/) do |table|
  table.raw.each do |year, price|
    within '#stock_values_table' do
      within "#year_#{year}" do
        expect(page).to have_content(year)
      end
      within "#price_#{year}" do
        expect(page).to have_content(price)
      end
    end
  end
end

Given(/^the stock growth is shown as a visual graph$/) do
  expect(page).to have_selector('#stockChart')
end

Given(/^the stock data must be saved into the database for later review$/) do
  expect(Stock.count).to_not eq(0)
end

Given(/^I click "(.*?)"$/) do |content|
  click_link(content)
end

Given(/^the system has already calculated stocks$/) do |table|
  table.raw.drop(1).each do |name, price, quantity, percentage, years|
    Stock.create(
      name:       name,
      price:      price,
      quantity:   quantity,
      percentage: percentage,
      years:      years
    )
  end
end

Given(/^I must see a table of saved stocks:$/) do |table|
  table.raw.each do |fields|
    fields.each do |content|
      expect(page).to have_content(content)
    end
  end
end

Given(/^I click on the calculated line "(.*?)"$/) do |content|
  click_link(content)
end

Given(/^I must see the already calculated data$/) do |table|
  table.raw.flatten.each do |content|
    expect(page).to have_content(content)
  end
end
